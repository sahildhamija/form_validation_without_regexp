function validation(){
    
    var user = document.getElementById('user').value;
    var pass = document.getElementById('pass').value;
    var confirmpass = document.getElementById('confpass').value;
    var mobileNumber = document.getElementById('mobileno').value;
    var emails = document.getElementById('emailid').value;





    if(user == ""){
        document.getElementById('username').innerHTML ="Username cannot be blank";
        return false;
    }
    if((user.length < 2) || (user.length > 20)) {
        document.getElementById('username').innerHTML ="Username must be greater than 2 characters and less than 20";
        return false;	
    }
    if(!isNaN(user)){
        document.getElementById('username').innerHTML ="Only characters are allowed";
        return false;
    }







    if(pass == ""){
        document.getElementById('passwords').innerHTML ="Password cannot be blank";
        return false;
    }
    if((pass.length <= 5) || (pass.length > 20)) {
        document.getElementById('passwords').innerHTML ="Password must be between 5 and 20 characters";
        return false;	
    }


    if(pass!=confirmpass){
        document.getElementById('confrmpass').innerHTML ="Password does not match with the confirm password";
        return false;
    }



    if(confirmpass == ""){
        document.getElementById('confrmpass').innerHTML ="Confirm password cannot be blank";
        return false;
    }




    if(mobileNumber == ""){
        document.getElementById('mobileno').innerHTML ="Mobile number is mandatory";
        return false;
    }
    if(isNaN(mobileNumber)){
        document.getElementById('mobileno').innerHTML ="Mobile number should be in digits";
        return false;
    }
    if(mobileNumber.length!=10){
        document.getElementById('mobileno').innerHTML ="Invalid mobile number, should be 10 digits ";
        return false;
    }



    if(emails == ""){
        document.getElementById('emailids').innerHTML ="Email id required for signing up";
        return false;
    }
    if(emails.indexOf('@') <= 0 ){
        document.getElementById('emailids').innerHTML ="@ is at invalid position";
        return false;
    }

    if((emails.charAt(emails.length-4)!='.') && (emails.charAt(emails.length-3)!='.')){
        document.getElementById('emailids').innerHTML =". is at invalid position";
        return false;
    }
}